# Basalt optimization

Summary of Basalt optimization code removing irrelevant lines as timers, stats, commented code, etc...

## Code

- Comments based on the euroc_config.json config settings

### sqrt_keypoint_vio.cpp

```c++

template <class Scalar_>
void SqrtKeypointVioEstimator<Scalar_>::optimize() {

    // Initialize a map with the states and poses of the window
    AbsOrderMap aom;
    for (const auto& kv : frame_poses) {
        aom.abs_order_map[kv.first] = std::make_pair(aom.total_size, POSE_SIZE);
        aom.total_size += POSE_SIZE;    // 6
        aom.items++;
    }
    for (const auto& kv : frame_states) {
        aom.abs_order_map[kv.first] = std::make_pair(aom.total_size, POSE_VEL_BIAS_SIZE);
        aom.total_size += POSE_VEL_BIAS_SIZE;   // 15
        aom.items++;
    }

    // Initialize a map with imu data
    ImuLinData<Scalar> ild = {g, gyro_bias_sqrt_weight, accel_bias_sqrt_weight, {}};
    for (const auto& kv : imu_meas) {
      ild.imu_meas[kv.first] = &kv.second;
    }

    // lambda of LM algorithm
    lambda = Scalar(config.vio_lm_lambda_initial); // 1e-4

    // Setup landmark blocks Options
    typename LinearizationBase<Scalar, POSE_SIZE>::Options lqr_options;
    lqr_options.lb_options.huber_parameter = huber_thresh;                  // 1.0
    lqr_options.lb_options.obs_std_dev = obs_std_dev;                       // 0.5
    lqr_options.linearization_type = config.vio_linearization_type;         // ABS_QR

    // Create landmark blocks
    std::unique_ptr<LinearizationBase<Scalar, POSE_SIZE>> lqr;
    lqr = LinearizationBase<Scalar, POSE_SIZE>::create(this, aom, lqr_options, &marg_data, &ild);

    // main optimization loop
    for (; it <= config.vio_max_iterations && !terminated;) {   // vio_max_iterations = 7

      // linearize residuals
      Scalar error_total = 0;
      /// TODO: add code
      error_total = lqr->linearizeProblem();

      // marginalize points in place
      /// TODO: add code
      lqr->performQR();

      // Second loop (??)
      for (int j = 0; it <= config.vio_max_iterations && !terminated; j++) {
        // get dense reduced camera system
        MatX H;
        VecX b;
        /// TODO: add code
        lqr->get_dense_H_b(H, b);

        // Solve the linearized system
        /// TODO: write this in natural lenguaje
        int iter = 0;
        bool inc_valid = false;
        constexpr int max_num_iter = 3;
        while (iter < max_num_iter && !inc_valid) {
          VecX Hdiag_lambda = (H.diagonal() * lambda).cwiseMax(min_lambda);
          MatX H_copy = H;
          H_copy.diagonal() += Hdiag_lambda;

          Eigen::LDLT<Eigen::Ref<MatX>> ldlt(H_copy);
          inc = ldlt.solve(b);

          if (!inc.array().isFinite().all()) {
            lambda = lambda_vee * lambda;
            lambda_vee *= vee_factor;
          } else
            inc_valid = true;
          iter++;
        }

        // backup state (then apply increment and check cost decrease)
        backup();

        // backsubstitute (with scaled pose increment)
        Scalar l_diff = 0;
        inc = -inc;
        /// TODO: add code
        l_diff = lqr->backSubstitute(inc);

        // apply increment to poses
        for (auto& [frame_id, state] : frame_poses) {
          int idx = aom.abs_order_map.at(frame_id).first;
          state.applyInc(inc.template segment<POSE_SIZE>(idx));
        }

        // apply increment to states
        for (auto& [frame_id, state] : frame_states) {
          int idx = aom.abs_order_map.at(frame_id).first;
          state.applyInc(inc.template segment<POSE_VEL_BIAS_SIZE>(idx));
        }

        // compute stepsize
        Scalar step_norminf = inc.array().abs().maxCoeff();

        // compute error update applying increment
        Scalar after_update_marg_prior_error = 0;
        Scalar after_update_vision_and_inertial_error = 0;
        {
          /// TODO: add code
          computeError(after_update_vision_and_inertial_error);             // 0
          computeMargPriorError(marg_data, after_update_marg_prior_error);  // 0
          Scalar after_update_imu_error = 0, after_bg_error = 0, after_ba_error = 0;
          ScBundleAdjustmentBase<Scalar>::computeImuError(
              aom,
              after_update_imu_error,                                       // 0
              after_bg_error,                                               // 0
              after_ba_error,                                               // 0
              frame_states,
              imu_meas,
              gyro_bias_sqrt_weight.array().square(),
              accel_bias_sqrt_weight.array().square(),
              g                                                             // Vector3d g(0, 0, -9.81)
          );
          after_update_vision_and_inertial_error += after_update_imu_error + after_bg_error + after_ba_error;
        }

        Scalar after_error_total = after_update_vision_and_inertial_error + after_update_marg_prior_error;

        // check cost decrease compared to quadratic model cost
        Scalar f_diff;
        bool step_is_valid = false;
        bool step_is_successful = false;
        Scalar relative_decrease = 0;

        // compute actual cost decrease
        {
          f_diff = error_total - after_error_total;
          relative_decrease = f_diff / l_diff;
          step_is_valid = l_diff > 0;
          step_is_successful = step_is_valid && relative_decrease > 0;
        }

        if (step_is_successful) {
          lambda *= std::max<Scalar>(Scalar(1.0) / 3, 1 - std::pow<Scalar>(2 * relative_decrease - 1, 3));
          lambda = std::max(min_lambda, lambda);                            // min_lambda = 1e-6
          lambda_vee = initial_vee;                                         // Scalar(2.0)

          // increment max iterations
          it++;

          // check function and parameter tolerance
          if ((f_diff > 0 && f_diff < Scalar(1e-6)) || step_norminf < Scalar(1e-4)) {
            converged = true;
            terminated = true;
          }

          // stop inner lm loop (j loop)
          break;
        } else {
          lambda = lambda_vee * lambda;
          lambda_vee *= vee_factor;

          // restore previous backup (before apply increment)
          restore();

          // increment max iterations
          it++;

          // Solver did not converge and reached maximum damping lambda
          if (lambda > max_lambda)                                          // max_lambda = 1e2
            terminated = true;

        }
      }     // close inner loop (j)
    }       // close main loop (i)
}

```

### linearization_base.cpp

```c++

template <typename Scalar_, int POSE_SIZE_>
std::unique_ptr<LinearizationBase<Scalar_, POSE_SIZE_>> LinearizationBase<Scalar_, POSE_SIZE_>::create(
    BundleAdjustmentBase<Scalar>* estimator, const AbsOrderMap& aom, const Options& options,
    const MargLinData<Scalar>* marg_lin_data, const ImuLinData<Scalar>* imu_lin_data,
    const std::set<FrameId>* used_frames, const std::unordered_set<KeypointId>* lost_landmarks,
    int64_t last_state_to_marg) {

  switch (options.linearization_type) {   // ABS_QR
    case LinearizationType::ABS_QR:
      return std::make_unique<LinearizationAbsQR<Scalar, POSE_SIZE>>(
          estimator, aom, options, marg_lin_data, imu_lin_data, used_frames, lost_landmarks, last_state_to_marg);

    case LinearizationType::ABS_SC:
      return std::make_unique<LinearizationAbsSC<Scalar, POSE_SIZE>>(
          estimator, aom, options, marg_lin_data, imu_lin_data, used_frames, lost_landmarks, last_state_to_marg);

    case LinearizationType::REL_SC:
      return std::make_unique<LinearizationRelSC<Scalar, POSE_SIZE>>(
          estimator, aom, options, marg_lin_data, imu_lin_data, used_frames, lost_landmarks, last_state_to_marg);

    default:
      std::cerr << "Could not select a valid linearization." << std::endl;
      std::abort();
  }
}

```

### linearization_abs_qr.cpp

```c++

LinearizationAbsQR<Scalar, POSE_SIZE>::LinearizationAbsQR(
  BundleAdjustmentBase<Scalar>* estimator,                    // this (SqrtKeypointVioEstimator)
  const AbsOrderMap& aom,                                     // aom
  const Options& options,                                     // lqr_options
  const MargLinData<Scalar>* marg_lin_data,                   // &marg_data
  const ImuLinData<Scalar>* imu_lin_data,                     // &ild
  const std::set<FrameId>* used_frames,                       // default = nullptr
  const std::unordered_set<KeypointId>* lost_landmarks,       // default = nullptr
  int64_t last_state_to_marg                                  // default = max()
  ) {

    // Allocate memory for relative pose linearization
    for (const auto& [tcid_h, target_map] : lmdb_.getObservations()) {
      const size_t host_idx = host_to_idx_.size();
      host_to_idx_.try_emplace(tcid_h, host_idx);
      host_to_landmark_block.try_emplace(tcid_h);

      for (const auto& [tcid_t, obs] : target_map) {
        std::pair<TimeCamId, TimeCamId> key(tcid_h, tcid_t);
        relative_pose_lin.emplace(key, RelPoseLin<Scalar>());
      }
    }

    // Populate lookup for relative poses grouped by host-frame
    /// NOTE: it's the same loop.. why isn't it unified?
    for (const auto& [tcid_h, target_map] : lmdb_.getObservations()) {
      relative_pose_per_host.emplace_back();

      for (const auto& [tcid_t, _] : target_map) {
        std::pair<TimeCamId, TimeCamId> key(tcid_h, tcid_t);
        auto it = relative_pose_lin.find(key);

        relative_pose_per_host.back().emplace_back(it);
      }
    }

    num_cameras = frame_poses.size();   // <= 7

    // Populate landmarks_ids
    landmark_ids.clear();
    for (const auto& [k, v] : lmdb_.getLandmarks()) {
      if (used_frames || lost_landmarks) {
        if (used_frames && used_frames->count(v.host_kf_id.frame_id)) {
          landmark_ids.emplace_back(k);
        } else if (lost_landmarks && lost_landmarks->count(k)) {
          landmark_ids.emplace_back(k);
        }
      } else {
        landmark_ids.emplace_back(k);
      }
    }
    size_t num_landmakrs = landmark_ids.size();

    // Populate landmark blocks
    landmark_blocks.resize(num_landmakrs);
    {
      auto body = [&](const tbb::blocked_range<size_t>& range) {
        for (size_t r = range.begin(); r != range.end(); ++r) {
          KeypointId lm_id = landmark_ids[r];
          auto& lb = landmark_blocks[r];
          auto& landmark = lmdb_.getLandmark(lm_id);

          lb = LandmarkBlock<Scalar>::template createLandmarkBlock<POSE_SIZE>();

          lb->allocateLandmark(landmark, relative_pose_lin, calib, aom, options.lb_options);
        }
      };

      tbb::blocked_range<size_t> range(0, num_landmakrs);
      tbb::parallel_for(range, body);
    }

    landmark_block_idx.reserve(num_landmakrs);
    num_rows_Q2r = 0;
    for (size_t i = 0; i < num_landmakrs; i++) {
      landmark_block_idx.emplace_back(num_rows_Q2r);
      const auto& lb = landmark_blocks[i];
      num_rows_Q2r += lb->numQ2rows();

      host_to_landmark_block.at(lb->getHostKf()).emplace_back(lb.get());
    }

    // Populate IMU blocks
    if (imu_lin_data) {
      for (const auto& kv : imu_lin_data->imu_meas) {
        imu_blocks.emplace_back(new ImuBlock<Scalar>(kv.second, imu_lin_data, aom));
      }
    }

}

template <typename Scalar, int POSE_SIZE>
Scalar LinearizationAbsQR<Scalar, POSE_SIZE>::linearizeProblem(bool* numerically_valid) {
  // reset damping and scaling (might be set from previous iteration)
  pose_damping_diagonal = 0;
  pose_damping_diagonal_sqrt = 0;
  marg_scaling = VecX();

  // Linearize relative poses
  // For each observation insert in the map relative_pose_lin: (keyframe host, keyframe target) -> RelPoseLin, where
  //    RelPoseLin {
  //      T_t_h;     Matrix 4x4:
  //      d_rel_d_h; Matrix 6x6: J  ??
  //      d_rel_d_t; Matrix 6x6: Jt ??
  //    };
  for (const auto& [tcid_h, target_map] : lmdb_.getObservations()) {

    for (const auto& [tcid_t, _] : target_map) {
      std::pair<TimeCamId, TimeCamId> key(tcid_h, tcid_t);
      RelPoseLin<Scalar>& rpl = relative_pose_lin.at(key);

      if (tcid_h != tcid_t) {
        const PoseStateWithLin<Scalar>& state_h = estimator->getPoseStateWithLin(tcid_h.frame_id);
        const PoseStateWithLin<Scalar>& state_t = estimator->getPoseStateWithLin(tcid_t.frame_id);

        // compute relative pose & Jacobians at linearization point
        Sophus::SE3<Scalar> T_t_h_sophus =
            computeRelPose(state_h.getPoseLin(), calib.T_i_c[tcid_h.cam_id], state_t.getPoseLin(),
                           calib.T_i_c[tcid_t.cam_id], &rpl.d_rel_d_h, &rpl.d_rel_d_t);

        // if either state is already linearized, then the current state
        // estimate is different from the linearization point, so recompute
        // the value (not Jacobian) again based on the current state.
        if (state_h.isLinearized() || state_t.isLinearized()) {
          T_t_h_sophus = computeRelPose(state_h.getPose(), calib.T_i_c[tcid_h.cam_id], state_t.getPose(),
                                        calib.T_i_c[tcid_t.cam_id]);
        }

        rpl.T_t_h = T_t_h_sophus.matrix();
      } else {
        rpl.T_t_h.setIdentity();
        rpl.d_rel_d_h.setZero();
        rpl.d_rel_d_t.setZero();
      }
    }
  }

  // Linearize landmarks
  size_t num_landmarks = landmark_blocks.size();

  auto body = [&](const tbb::blocked_range<size_t>& range, std::pair<Scalar, bool> error_valid) {
    for (size_t r = range.begin(); r != range.end(); ++r) {
      error_valid.first += landmark_blocks[r]->linearizeLandmark();
      error_valid.second = error_valid.second && !landmark_blocks[r]->isNumericalFailure();
    }
    return error_valid;
  };

  std::pair<Scalar, bool> initial_value = {0.0, true};
  auto join = [](auto p1, auto p2) {
    p1.first += p2.first;
    p1.second = p1.second && p2.second;
    return p1;
  };

  tbb::blocked_range<size_t> range(0, num_landmarks);
  auto reduction_res = tbb::parallel_reduce(range, initial_value, body, join);

  if (numerically_valid) *numerically_valid = reduction_res.second;

  if (imu_lin_data) {
    for (auto& imu_block : imu_blocks) {
      reduction_res.first += imu_block->linearizeImu(estimator->frame_states);
    }
  }

  if (marg_lin_data) {
    Scalar marg_prior_error;
    estimator->computeMargPriorError(*marg_lin_data, marg_prior_error);
    reduction_res.first += marg_prior_error;
  }

  return reduction_res.first;
}

```

## Classes and Structures

```c++

struct AbsOrderMap {
  std::map<int64_t, std::pair<int, int>> abs_order_map;
  size_t items = 0;
  size_t total_size = 0;
};

struct ImuLinData {
  const Eigen::Matrix<Scalar, 3, 1>& g;
  const Eigen::Matrix<Scalar, 3, 1>& gyro_bias_weight_sqrt;
  const Eigen::Matrix<Scalar, 3, 1>& accel_bias_weight_sqrt;

  std::map<int64_t, const IntegratedImuMeasurement<Scalar>*> imu_meas;
};


struct LinearizationBase::Options {
    typename LandmarkBlock<Scalar>::Options lb_options;
    LinearizationType linearization_type;
  };

struct LandmarkBlock::Options {
    // use Householder instead of Givens for marginalization
    bool use_householder = true;

    // use_valid_projections_only: if true, set invalid projection's
    // residual and jacobian to 0; invalid means z <= 0
    bool use_valid_projections_only = true;

    // if > 0, use huber norm with given threshold, else squared norm
    Scalar huber_parameter = 0;

    // Standard deviation of reprojection error to weight visual measurements
    Scalar obs_std_dev = 1;

    // ceres uses 1.0 / (1.0 + sqrt(SquaredColumnNorm))
    // we use 1.0 / (eps + sqrt(SquaredColumnNorm))
    Scalar jacobi_scaling_eps = 1e-6;
  };

enum class LinearizationType { ABS_QR, ABS_SC, REL_SC };

// LinearizationAbsQR just variables
class LinearizationAbsQR : public LinearizationBase<Scalar_, POSE_SIZE_> {
 protected:

  Options options_;

  std::vector<KeypointId> landmark_ids;
  std::vector<LandmarkBlockPtr> landmark_blocks;
  std::vector<ImuBlockPtr> imu_blocks;

  std::unordered_map<TimeCamId, size_t> host_to_idx_;
  HostLandmarkMapType host_to_landmark_block;

  std::vector<size_t> landmark_block_idx;
  const BundleAdjustmentBase<Scalar>* estimator;

  LandmarkDatabase<Scalar>& lmdb_;
  const Eigen::aligned_map<int64_t, PoseStateWithLin<Scalar>>& frame_poses;

  const Calibration<Scalar>& calib;

  const AbsOrderMap& aom;
  const std::set<FrameId>* used_frames;

  const MargLinData<Scalar>* marg_lin_data;
  const ImuLinData<Scalar>* imu_lin_data;

  PoseLinMapType relative_pose_lin;
  std::vector<std::vector<PoseLinMapTypeConstIter>> relative_pose_per_host;

  Scalar pose_damping_diagonal;
  Scalar pose_damping_diagonal_sqrt;

  VecX marg_scaling;

  size_t num_cameras;
  size_t num_rows_Q2r;
}

struct RelPoseLin {
  Eigen::Matrix<Scalar, 4, 4> T_t_h;
  Eigen::Matrix<Scalar, 6, 6> d_rel_d_h;
  Eigen::Matrix<Scalar, 6, 6> d_rel_d_t;
};

```
