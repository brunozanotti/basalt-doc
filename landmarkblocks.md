# Basalt optimization

Summary of Basalt Landmark Blocks code removing irrelevant lines as timers, stats, commented code, etc...

## Code

- Comments based on the euroc_config.json config settings
- The main class LandmarkBlock is defined in landmark_block.hpp file
- The implementation is in landmark_block_abs_dynamic.hpp

### landmark_block_abs_dynamic.hpp: allocateLandmark

This function is called from the constructor of the linearization class

```c++
  virtual inline void allocateLandmark(
      Landmark<Scalar>& lm,
      const Eigen::aligned_unordered_map<std::pair<TimeCamId, TimeCamId>, RelPoseLin<Scalar>>& relative_pose_lin,
      const Calibration<Scalar>& calib, const AbsOrderMap& aom, const Options& options,
      const std::map<TimeCamId, size_t>* rel_order = nullptr) override {
    // some of the logic assumes the members are at their initial values
    BASALT_ASSERT(state == State::Uninitialized);

    UNUSED(rel_order);

    lm_ptr = &lm;
    options_ = &options;
    calib_ = &calib;

    // TODO: consider for VIO that we have a lot of 0 columns if we just use aom
    // --> add option to use different AOM with reduced size and/or just
    // involved poses --> when accumulating results, check which case we have;
    // if both aom are identical, we don't have to do block-wise operations.
    aom_ = &aom;

    pose_lin_vec.clear();
    pose_lin_vec.reserve(lm.obs.size());
    pose_tcid_vec.clear();
    pose_tcid_vec.reserve(lm.obs.size());

    // LMBs without host frame should not be created
    BASALT_ASSERT(aom.abs_order_map.count(lm.host_kf_id.frame_id) > 0);

    for (const auto& [tcid_t, pos] : lm.obs) {
      size_t i = pose_lin_vec.size();

      auto it = relative_pose_lin.find(std::make_pair(lm.host_kf_id, tcid_t));
      BASALT_ASSERT(it != relative_pose_lin.end());

      if (aom.abs_order_map.count(tcid_t.frame_id) > 0) {
        pose_lin_vec.push_back(&it->second);
      } else {
        // Observation droped for marginalization
        pose_lin_vec.push_back(nullptr);
      }
      pose_tcid_vec.push_back(&it->first);

      res_idx_by_abs_pose_[it->first.first.frame_id].insert(i);   // host
      res_idx_by_abs_pose_[it->first.second.frame_id].insert(i);  // target
    }

    // number of pose-jacobian columns is determined by oam
    // padding_idx: 87 = POSE_SIZE * vio_max_kfs + POSE_VEL_BIAS_SIZE * vio_max_states = 6*7+15*3
    padding_idx = aom_->total_size;

    // residuals and lm damping (??)
    // num_rows: 9, 11, 13, 19, 23, 27, 31, 35
    // pose_lin_vec.size(): 3, 4, 5, 8, 10, 12, 14, 16
    num_rows = pose_lin_vec.size() * 2 + 3;

    size_t pad = padding_idx % 4; // 87 % 4 = 3
    if (pad != 0) {
      padding_size = 4 - pad; // 1
    }

    lm_idx = padding_idx + padding_size; // 88

    res_idx = lm_idx + 3;   // 91

    num_cols = res_idx + 1; // 92

    // number of columns should now be multiple of 4 for good memory alignment
    // TODO: test extending this to 8 --> 32byte alignment for float?
    BASALT_ASSERT(num_cols % 4 == 0);

    storage.resize(num_rows, num_cols); // (92)

    damping_rotations.clear();
    damping_rotations.reserve(6);

    state = State::Allocated;
  }

```

### linearization_abs_qr.cpp: linearizeProblem

```c++


```

### linearization_abs_qr.cpp: performQR

```c++

```

## Main landmark block structure

```c++

class LandmarkBlockAbsDynamic : public LandmarkBlock<Scalar> {

  // Dense storage for pose Jacobians, padding, landmark Jacobians and
  // residuals [J_p | pad | J_l | res]
  Eigen::Matrix<Scalar, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor> storage;

  Vec3 Jl_col_scale = Vec3::Ones();
  std::vector<Eigen::JacobiRotation<Scalar>> damping_rotations;

  std::vector<const RelPoseLin<Scalar>*> pose_lin_vec;
  std::vector<const std::pair<TimeCamId, TimeCamId>*> pose_tcid_vec;

  // number of pose-jacobian columns
  size_t padding_idx = 0;
  size_t padding_size = 0;
  size_t lm_idx = 0;
  size_t res_idx = 0;

  size_t num_cols = 0;
  size_t num_rows = 0;

  const Options* options_ = nullptr;

  State state = State::Uninitialized;

  // Landmark pointer
  Landmark<Scalar>* lm_ptr = nullptr;

  // Calibration
  const Calibration<Scalar>* calib_ = nullptr;

  // Map with states and poses
  const AbsOrderMap* aom_ = nullptr;

  std::map<int64_t, std::set<int>> res_idx_by_abs_pose_;
};

```

## Other Classes and Structures

```c++

enum State { Uninitialized = 0, Allocated, NumericalFailure, Linearized, Marginalized };

struct LandmarkBlock::Options {
    // use Householder instead of Givens for marginalization
    bool use_householder = true;

    // use_valid_projections_only: if true, set invalid projection's
    // residual and jacobian to 0; invalid means z <= 0
    bool use_valid_projections_only = true;

    // if > 0, use huber norm with given threshold, else squared norm
    Scalar huber_parameter = 0;

    // Standard deviation of reprojection error to weight visual measurements
    Scalar obs_std_dev = 1;

    // ceres uses 1.0 / (1.0 + sqrt(SquaredColumnNorm))
    // we use 1.0 / (eps + sqrt(SquaredColumnNorm))
    Scalar jacobi_scaling_eps = 1e-6;
  };

```
