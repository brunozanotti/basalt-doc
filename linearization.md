# Basalt optimization

Summary of Basalt linearization code removing irrelevant lines as timers, stats, commented code, etc...

Basalt has multiple linearization types "ABS_QR", "ABS_SC" and "REL_SC", here we focus on "ABS_QR" that was implemented in the paper "ICCV'21 square root marginalization paper".

## Code

- Comments based on the euroc_config.json config settings

### linearization_abs_qr.cpp: Create

```c++

LinearizationAbsQR<Scalar, POSE_SIZE>::LinearizationAbsQR(
  BundleAdjustmentBase<Scalar>* estimator,                    // this (SqrtKeypointVioEstimator)
  const AbsOrderMap& aom,                                     // aom
  const Options& options,                                     // lqr_options
  const MargLinData<Scalar>* marg_lin_data,                   // &marg_data
  const ImuLinData<Scalar>* imu_lin_data,                     // &ild
  const std::set<FrameId>* used_frames,                       // default = nullptr
  const std::unordered_set<KeypointId>* lost_landmarks,       // default = nullptr
  int64_t last_state_to_marg                                  // default = max()
  ) {

    // Allocate memory for relative pose linearization
    for (const auto& [tcid_h, target_map] : lmdb_.getObservations()) {
      const size_t host_idx = host_to_idx_.size();
      host_to_idx_.try_emplace(tcid_h, host_idx);
      host_to_landmark_block.try_emplace(tcid_h);

      for (const auto& [tcid_t, obs] : target_map) {
        std::pair<TimeCamId, TimeCamId> key(tcid_h, tcid_t);
        relative_pose_lin.emplace(key, RelPoseLin<Scalar>());
      }
    }

    // Populate lookup for relative poses grouped by host-frame
    /// NOTE: it's the same loop.. why isn't it unified?
    for (const auto& [tcid_h, target_map] : lmdb_.getObservations()) {
      relative_pose_per_host.emplace_back();

      for (const auto& [tcid_t, _] : target_map) {
        std::pair<TimeCamId, TimeCamId> key(tcid_h, tcid_t);
        auto it = relative_pose_lin.find(key);

        relative_pose_per_host.back().emplace_back(it);
      }
    }

    num_cameras = frame_poses.size();   // <= 7

    // Populate landmarks_ids
    landmark_ids.clear();
    for (const auto& [k, v] : lmdb_.getLandmarks()) {
      if (used_frames || lost_landmarks) {
        if (used_frames && used_frames->count(v.host_kf_id.frame_id)) {
          landmark_ids.emplace_back(k);
        } else if (lost_landmarks && lost_landmarks->count(k)) {
          landmark_ids.emplace_back(k);
        }
      } else {
        landmark_ids.emplace_back(k);
      }
    }
    size_t num_landmakrs = landmark_ids.size();

    // Populate landmark blocks
    landmark_blocks.resize(num_landmakrs);
    {
      auto body = [&](const tbb::blocked_range<size_t>& range) {
        for (size_t r = range.begin(); r != range.end(); ++r) {
          KeypointId lm_id = landmark_ids[r];
          auto& lb = landmark_blocks[r];
          auto& landmark = lmdb_.getLandmark(lm_id);

          lb = LandmarkBlock<Scalar>::template createLandmarkBlock<POSE_SIZE>();

          lb->allocateLandmark(landmark, relative_pose_lin, calib, aom, options.lb_options);
        }
      };

      tbb::blocked_range<size_t> range(0, num_landmakrs);
      tbb::parallel_for(range, body);
    }

    landmark_block_idx.reserve(num_landmakrs);
    num_rows_Q2r = 0;
    for (size_t i = 0; i < num_landmakrs; i++) {
      landmark_block_idx.emplace_back(num_rows_Q2r);
      const auto& lb = landmark_blocks[i];
      num_rows_Q2r += lb->numQ2rows();

      host_to_landmark_block.at(lb->getHostKf()).emplace_back(lb.get());
    }

    // Populate IMU blocks
    if (imu_lin_data) {
      for (const auto& kv : imu_lin_data->imu_meas) {
        imu_blocks.emplace_back(new ImuBlock<Scalar>(kv.second, imu_lin_data, aom));
      }
    }

}

```

### linearization_abs_qr.cpp: linearizeProblem

```c++

template <typename Scalar, int POSE_SIZE>
Scalar LinearizationAbsQR<Scalar, POSE_SIZE>::linearizeProblem(bool* numerically_valid) {
  // reset damping and scaling (might be set from previous iteration)
  pose_damping_diagonal = 0;
  pose_damping_diagonal_sqrt = 0;
  marg_scaling = VecX();

  // Linearize relative poses
  // For each observation insert in the map relative_pose_lin: (keyframe host, keyframe target) -> RelPoseLin
  for (const auto& [tcid_h, target_map] : lmdb_.getObservations()) {

    for (const auto& [tcid_t, _] : target_map) {
      std::pair<TimeCamId, TimeCamId> key(tcid_h, tcid_t);
      RelPoseLin<Scalar>& rpl = relative_pose_lin.at(key);

      if (tcid_h != tcid_t) {
        const PoseStateWithLin<Scalar>& state_h = estimator->getPoseStateWithLin(tcid_h.frame_id);
        const PoseStateWithLin<Scalar>& state_t = estimator->getPoseStateWithLin(tcid_t.frame_id);

        // compute relative pose & Jacobians at linearization point
        Sophus::SE3<Scalar> T_t_h_sophus =
            computeRelPose(state_h.getPoseLin(), calib.T_i_c[tcid_h.cam_id], state_t.getPoseLin(),
                           calib.T_i_c[tcid_t.cam_id], &rpl.d_rel_d_h, &rpl.d_rel_d_t)

        // if either state is already linearized, then the current state
        // estimate is different from the linearization point, so recompute
        // the value (not Jacobian) again based on the current state.
        if (state_h.isLinearized() || state_t.isLinearized()) {
          T_t_h_sophus = computeRelPose(state_h.getPose(), calib.T_i_c[tcid_h.cam_id], state_t.getPose(),
                                        calib.T_i_c[tcid_t.cam_id]);
        }

        rpl.T_t_h = T_t_h_sophus.matrix();
      } else {
        rpl.T_t_h.setIdentity();
        rpl.d_rel_d_h.setZero();
        rpl.d_rel_d_t.setZero();
      }
    }
  }

  // Linearize landmarks
  size_t num_landmarks = landmark_blocks.size();

  auto body = [&](const tbb::blocked_range<size_t>& range, std::pair<Scalar, bool> error_valid) {
    for (size_t r = range.begin(); r != range.end(); ++r) {
      error_valid.first += landmark_blocks[r]->linearizeLandmark();
      error_valid.second = error_valid.second && !landmark_blocks[r]->isNumericalFailure();
    }
    return error_valid;
  };

  std::pair<Scalar, bool> initial_value = {0.0, true};
  auto join = [](auto p1, auto p2) {
    p1.first += p2.first;
    p1.second = p1.second && p2.second;
    return p1;
  };

  tbb::blocked_range<size_t> range(0, num_landmarks);
  auto reduction_res = tbb::parallel_reduce(range, initial_value, body, join);

  if (numerically_valid) *numerically_valid = reduction_res.second;

  if (imu_lin_data) {
    for (auto& imu_block : imu_blocks) {
      reduction_res.first += imu_block->linearizeImu(estimator->frame_states);
    }
  }

  if (marg_lin_data) {
    Scalar marg_prior_error;
    estimator->computeMargPriorError(*marg_lin_data, marg_prior_error);
    reduction_res.first += marg_prior_error;
  }

  return reduction_res.first;
}

Sophus::SE3<Scalar> computeRelPose(const Sophus::SE3<Scalar>& T_w_i_h, const Sophus::SE3<Scalar>& T_i_c_h,
                                   const Sophus::SE3<Scalar>& T_w_i_t, const Sophus::SE3<Scalar>& T_i_c_t,
                                   Sophus::Matrix6<Scalar>* d_rel_d_h = nullptr,
                                   Sophus::Matrix6<Scalar>* d_rel_d_t = nullptr) {
  Sophus::SE3<Scalar> tmp2 = (T_i_c_t).inverse();

  Sophus::SE3<Scalar> T_t_i_h_i;
  T_t_i_h_i.so3() = T_w_i_t.so3().inverse() * T_w_i_h.so3();
  T_t_i_h_i.translation() = T_w_i_t.so3().inverse() * (T_w_i_h.translation() - T_w_i_t.translation());

  Sophus::SE3<Scalar> tmp = tmp2 * T_t_i_h_i;
  Sophus::SE3<Scalar> res = tmp * T_i_c_h; // T_t_c_t_i * T_t_i_h_i * T_h_i_h_c = T_t_c_h_c

  if (d_rel_d_h) {
    Sophus::Matrix3<Scalar> R = T_w_i_h.so3().inverse().matrix();

    Sophus::Matrix6<Scalar> RR;
    RR.setZero();
    RR.template topLeftCorner<3, 3>() = R;
    RR.template bottomRightCorner<3, 3>() = R;

    *d_rel_d_h = tmp.Adj() * RR;
  }

  if (d_rel_d_t) {
    Sophus::Matrix3<Scalar> R = T_w_i_t.so3().inverse().matrix();

    Sophus::Matrix6<Scalar> RR;
    RR.setZero();
    RR.template topLeftCorner<3, 3>() = R;
    RR.template bottomRightCorner<3, 3>() = R;

    *d_rel_d_t = -tmp2.Adj() * RR;
  }

  return res;
}

```

### linearization_abs_qr.cpp: performQR

```c++
template <typename Scalar, int POSE_SIZE>
void LinearizationAbsQR<Scalar, POSE_SIZE>::performQR() {
  auto body = [&](const tbb::blocked_range<size_t>& range) {
    for (size_t r = range.begin(); r != range.end(); ++r) {
      landmark_blocks[r]->performQR();
    }
  };

  tbb::blocked_range<size_t> range(0, landmark_block_idx.size());
  tbb::parallel_for(range, body);
}

```

## Main linearization class

```c++

// LinearizationAbsQR just variables
class LinearizationAbsQR : {

  Options options_;

  std::vector<KeypointId> landmark_ids;
  std::vector<LandmarkBlockPtr> landmark_blocks;
  std::vector<ImuBlockPtr> imu_blocks;

  std::unordered_map<TimeCamId, size_t> host_to_idx_;
  HostLandmarkMapType host_to_landmark_block;

  std::vector<size_t> landmark_block_idx;
  const BundleAdjustmentBase<Scalar>* estimator;

  LandmarkDatabase<Scalar>& lmdb_;
  const Eigen::aligned_map<int64_t, PoseStateWithLin<Scalar>>& frame_poses;

  const Calibration<Scalar>& calib;

  const AbsOrderMap& aom;
  const std::set<FrameId>* used_frames;

  const MargLinData<Scalar>* marg_lin_data;
  const ImuLinData<Scalar>* imu_lin_data;

  // Map of (host keyframe, target keyframe) -> RelPoseLin
  PoseLinMapType relative_pose_lin;

  std::vector<std::vector<PoseLinMapTypeConstIter>> relative_pose_per_host;

  Scalar pose_damping_diagonal;
  Scalar pose_damping_diagonal_sqrt;

  VecX marg_scaling;

  size_t num_cameras;
  size_t num_rows_Q2r;
}

```

## Other Classes and Structures

```c++

struct LandmarkBlock::Options {
    // use Householder instead of Givens for marginalization
    bool use_householder = true;

    // use_valid_projections_only: if true, set invalid projection's
    // residual and jacobian to 0; invalid means z <= 0
    bool use_valid_projections_only = true;

    // if > 0, use huber norm with given threshold, else squared norm
    Scalar huber_parameter = 0;

    // Standard deviation of reprojection error to weight visual measurements
    Scalar obs_std_dev = 1;

    // ceres uses 1.0 / (1.0 + sqrt(SquaredColumnNorm))
    // we use 1.0 / (eps + sqrt(SquaredColumnNorm))
    Scalar jacobi_scaling_eps = 1e-6;
  };


struct RelPoseLin {
  Eigen::Matrix<Scalar, 4, 4> T_t_h;      // Transformation from keyframe target to keyframe host
  Eigen::Matrix<Scalar, 6, 6> d_rel_d_h;  // Jacobian host
  Eigen::Matrix<Scalar, 6, 6> d_rel_d_t;  // Jacobian target
};

```
